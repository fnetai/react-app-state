import React from "react";
import { Provider, useSelector } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import reducer from "./reducer";

const store = configureStore({
    reducer: reducer,
    devTools: true
});

const ReduxProvider = ({ children }) => {
    return (
        <React.Fragment>
            <Provider store={store}>{children}</Provider>
        </React.Fragment>
    )
}

export { ReduxProvider as Provider, store as Store, useSelector };