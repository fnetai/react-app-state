import React from 'react';
import Store, { Action, Provider, useSelector } from '../src';

const Test = () => {
    const somepath = useSelector(state => state.default.a?.b?.c);

    React.useEffect(() => {
        setInterval(() => {
            Store.dispatch(Action.update('a.b.c', new Date().toISOString()));
        }, 1000);
    }, []);

    return (
        <h1>{somepath}</h1>
    );
}

export default (props) => {
    return (
        <Provider>
            <Test {...props} />
        </Provider>
    );
}